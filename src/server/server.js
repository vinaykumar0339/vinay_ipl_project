const express = require('express');
const fs = require('fs');
const path = require('path');
const PUBLIC_FOLDER_PATH = path.resolve(__dirname, '../public/');
const JSON_FILES_FOLDER = path.join(PUBLIC_FOLDER_PATH, '/output');
const sqlRouter = require('./routers/sqlRouters');
const {port: PORT} = require('./config');

const app = express();

app.use('/', express.static(path.join(__dirname, '../public')));

app.get('/output/:fileName', (req, res) => {
    let fileName = req.params.fileName;
    fs.readFile(path.join(JSON_FILES_FOLDER + '/' + fileName), 'utf8', (err, fileJson) => {
        if (err) {
            res.status(500);
            res.end();
            console.log(err);
        } else {
            res.set({
                'Content-Type': 'application/json'
            });
            res.status(200);
            res.send(fileJson);
        }
    })
});

app.use('/database', sqlRouter);

app.listen(PORT, () => {
    console.log(`listening on port ${PORT}`);
});