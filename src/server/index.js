const { 
        matchesPerYear, 
        matchesWonPerTeamPerYear, 
        extraRunsPerTeamByYear,
        top10EconomicalBowlersByYear,
        eachTeamWonTheMatchAndToss,
        highestNumberOfAwards,
        strikeRateOfBatsman,
        highestNumOfTimesPlayerDismissed,
        bowlerWithBestEconomy } = require('./ipl');
const path = require('path');
const csv = require('csvtojson');
const fs = require('fs');
const BASE_PATH = path.join(__dirname,'../');
const MATCHES_CSV_FILE = BASE_PATH + '/data/matches.csv';
const DELIVERIES_CSV_FILE = BASE_PATH + '/data/deliveries.csv';
const OUTPUT_DIRECTORY_PATH = BASE_PATH + '/public/output/';



function index() {
    csv()
        .fromFile(MATCHES_CSV_FILE)
        .then((matches) => {
            let numOfMatches = matchesPerYear(matches);
            let numOfMatchesWonPerTeam = matchesWonPerTeamPerYear(matches);
            let eachTeamWonTheMatch = eachTeamWonTheMatchAndToss(matches);
            let highestNumberOfAward = highestNumberOfAwards(matches);

            csv()
                .fromFile(DELIVERIES_CSV_FILE)
                .then((deliveries) => {
                console.log(deliveries[0]);

                    let extraRunsPerTeam = extraRunsPerTeamByYear(matches, deliveries, 2016);
                    let top10EconomicalBowlers = top10EconomicalBowlersByYear(matches, deliveries, 2015);
                    let strikeRateOfBatsmanAllYears = strikeRateOfBatsman(matches, deliveries);
                    let highestNumOfTimesPlayerDismissedForAllYear = highestNumOfTimesPlayerDismissed(deliveries);
                    let bowlerWithBestEconomyInSuperover = bowlerWithBestEconomy(deliveries);
                    saveJsonOutput(
                            numOfMatches, 
                            numOfMatchesWonPerTeam, 
                            extraRunsPerTeam, 
                            top10EconomicalBowlers,
                            eachTeamWonTheMatch,
                            highestNumberOfAward,
                            strikeRateOfBatsmanAllYears,
                            highestNumOfTimesPlayerDismissedForAllYear,
                            bowlerWithBestEconomyInSuperover);

                })
                .catch((err) => {
                    console.log(err);
                })
        })
        .catch((err) => {
            console.log(err);
        })
}

/**
 * 
 * This function saves the output json file.
 * 
 * @param {Object} numOfMatches - object of number of matches won
 * @param {Object} numOfMatchesWonPerTeam - object of number of matches won per team
 * @param {Object} extraRunsPerTeam - object of extra runs conceded per team
 * @param {Object} top10EconomicalBowlers - object of top 10 economical bowlers
 * @param {Object} eachTeamWonTheMatch - object of each team won the match
 * @param {Object} highestNumberOfAward - object of highest number of awards
 * @param {Object} strikeRateOfBatsmanAllYears - object of strike rate of batsman of all years
 * @param {Object} highestNumOfTimesPlayerDismissedForAllYear - object of highest number of times player dismissed for all years
 * @param {Object} bowlerWithBestEconomyInSuperover - object of bowler with best economy in super over
 */
function saveJsonOutput(numOfMatches, 
                        numOfMatchesWonPerTeam, 
                        extraRunsPerTeam, 
                        top10EconomicalBowlers,
                        eachTeamWonTheMatch,
                        highestNumberOfAward,
                        strikeRateOfBatsmanAllYears,
                        highestNumOfTimesPlayerDismissedForAllYear,
                        bowlerWithBestEconomyInSuperover) {
    numOfMatches = JSON.stringify(numOfMatches);
    numOfMatchesWonPerTeam = JSON.stringify(numOfMatchesWonPerTeam);
    extraRunsPerTeam = JSON.stringify(extraRunsPerTeam);
    top10EconomicalBowlers = JSON.stringify(top10EconomicalBowlers);
    eachTeamWonTheMatch = JSON.stringify(eachTeamWonTheMatch);
    highestNumberOfAward = JSON.stringify(highestNumberOfAward);
    strikeRateOfBatsmanAllYears = JSON.stringify(strikeRateOfBatsmanAllYears);
    highestNumOfTimesPlayerDismissedForAllYear = JSON.stringify(highestNumOfTimesPlayerDismissedForAllYear);
    bowlerWithBestEconomyInSuperover = JSON.stringify(bowlerWithBestEconomyInSuperover);

    fs.writeFile(OUTPUT_DIRECTORY_PATH + 'matchesPerYear.json', numOfMatches , 'utf8',(err) => {
        if(err) {
            console.log(err);
        }
    })
    fs.writeFile(OUTPUT_DIRECTORY_PATH + 'matchesWonPerTeamPerYear.json', numOfMatchesWonPerTeam , 'utf8',(err) => {
        if(err) {
            console.log(err);
        }
    })
    fs.writeFile(OUTPUT_DIRECTORY_PATH + 'extraRunsPerTeam.json', extraRunsPerTeam , 'utf8',(err) => {
        if(err) {
            console.log(err);
        }
    })
    fs.writeFile(OUTPUT_DIRECTORY_PATH + 'top10EconomicalBowlers.json', top10EconomicalBowlers , 'utf8',(err) => {
        if(err) {
            console.log(err);
        }
    })
    fs.writeFile(OUTPUT_DIRECTORY_PATH + 'eachTeamWonTheMatch.json', eachTeamWonTheMatch , 'utf8',(err) => {
        if(err) {
            console.log(err);
        }
    })
    fs.writeFile(OUTPUT_DIRECTORY_PATH + 'highestNumberOfAward.json', highestNumberOfAward , 'utf8',(err) => {
        if(err) {
            console.log(err);
        }
    })
    fs.writeFile(OUTPUT_DIRECTORY_PATH + 'strikeRateOfBatsmanAllYears.json', strikeRateOfBatsmanAllYears , 'utf8',(err) => {
        if(err) {
            console.log(err);
        }
    })
    fs.writeFile(OUTPUT_DIRECTORY_PATH + 'highestNumOfTimesPlayerDismissedForAllYear.json', highestNumOfTimesPlayerDismissedForAllYear , 'utf8',(err) => {
        if(err) {
            console.log(err);
        }
    })
    fs.writeFile(OUTPUT_DIRECTORY_PATH + 'bowlerWithBestEconomyInSuperover.json', bowlerWithBestEconomyInSuperover , 'utf8',(err) => {
        if(err) {
            console.log(err);
        }
    })
}

index();
