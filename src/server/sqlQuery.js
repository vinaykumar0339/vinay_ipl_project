const con = require('./connection');
const fs = require('fs');
const path = require('path');

function getDataFromDatabase(sqlFileName, year) {
    return new Promise((resolve, reject) => {
        fs.readFile(path.resolve(__dirname, `./queries/${sqlFileName}.sql`), 'utf-8', (err, sql) => {
            if (err) {
                reject(err);
            } else {
                if (year && typeof year == 'number') {
                    con.getConnection((err, connection) => {
                        if(err) {
                            console.log(err);
                            reject(err);
                        }else {
                            connection.query(sql, year,(err, result, fields) => {
                                connection.release();
                                if (err) {
                                    reject(err);
                                } else {
                                    resolve(result);
                                }
                            })
                        }
                    })
                } else {
                    con.getConnection((err, connection) => {
                        if(err) {
                            console.log(err);
                            reject(err);
                            
                        }else {
                            connection.query(sql,(err, result, fields) => {
                                connection.release();
                                if (err) {
                                    reject(err);
                                } else {
                                    resolve(result);
                                }
                            })
                        }
                    })
                }

            }
        })
    })
}

module.exports = getDataFromDatabase