const express = require('express');
const getDataFromDatabase = require('../sqlQuery');

const router = express.Router();


router.get('/matchesPerYear', (req, res) => {
    getDataFromDatabase('matchesPerYear')
        .then(data => {
            res.status(200).json(data);
            res.end();
        })
        .catch(err => {
            res.status(500).json({ 
                message : "internal server error"
            });
            res.end();
        })
})

router.get('/matchesWonPerTeamPerYear', (req, res) => {
    getDataFromDatabase('matchesWonPerTeamPerYear')
        .then(data => {
            res.status(200).json(data);
            res.end();
        })
        .catch(err => {
            res.status(500).json({ 
                message : "internal server error"
            });
            res.end();
        })
})

router.get('/eachTeamWonTheMatch', (req, res) => {
    getDataFromDatabase('eachTeamWonTheMatch')
        .then(data => {
            res.status(200).json(data);
            res.end();
        })
        .catch(err => {
            res.status(500).json({ 
                message : "internal server error"
            });
            res.end();
        })
})

router.get('/extraRunsPerTeam', (req, res) => {
    let year = parseInt(req.query.year) || 2015;
    getDataFromDatabase('extraRunsPerTeam', year)
        .then(data => {
            res.status(200).json(data);
            res.end();
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ 
                message : "internal server error"
            });
            res.end();
        })
})

router.get('/highestNumberOfAward', (req, res) => {
    getDataFromDatabase('highestNumberOfAward')
        .then(data => {
            res.status(200).json(data);
            res.end();
        })
        .catch(err => {
            res.status(500).json({ 
                message : "internal server error"
            });
            res.end();
        })
})

router.get('/highestNumOfTimesPlayerDismissedForAllYear', (req, res) => {
    getDataFromDatabase('highestNumOfTimesPlayerDismissedForAllYear')
        .then(data => {
            res.status(200).json(data);
            res.end();
        })
        .catch(err => {
            res.status(500).json({ 
                message : "internal server error"
            });
            res.end();
        })
})

router.get('/strikeRateOfBatsmanAllYears', (req, res) => {
    getDataFromDatabase('strikeRateOfBatsmanAllYears')
        .then(data => {
            res.status(200).json(data);
            res.end();
        })
        .catch(err => {
            res.status(500).json({ 
                message : "internal server error"
            });
            res.end();
        })
})

router.get('/top10EconomicalBowlers', (req, res) => {
    let year = parseInt(req.query.year) || 2015;
    getDataFromDatabase('top10EconomicalBowlers', year)
        .then(data => {
            res.status(200).json(data);
            res.end();
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ 
                message : "internal server error"
            });
            res.end();
        })
})

router.get('/bowlerWithBestEconomyInSuperover', (req, res) => {
    getDataFromDatabase('bowlerWithBestEconomyInSuperover')
        .then(data => {
            res.status(200).json(data);
            res.end();
        })
        .catch(err => {
            res.status(500).json({ 
                message : "internal server error"
            });
            res.end();
        })
})

module.exports = router;