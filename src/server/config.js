const dotenv = require('dotenv');
dotenv.config();

module.exports = {
    port: process.env.PORT || 3000,
    mysql: {
        host: process.env.PD_DB_HOST,
        user: process.env.PD_DB_USER,
        password: process.env.PD_DB_PASS,
        database: process.env.PD_DB_DATABASE,
        connectionLimit: 10
    }
};