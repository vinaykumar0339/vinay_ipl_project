/**
 * 
 * The function calculates the number of matches played per year for all the years in IPL.
 * 
 * @param {Object[]} matches - takes array of object
 * 
 * @returns {Object} - matches Per Year Object
 */
function matchesPerYear(matches) {
    let result = {};
    
    // iterating through the matches array of object
    matches.forEach(({season}) => {
        if (result[season]) { // checking that season key is having or not in result obj
            result[season] += 1; // incrementing the season count
        }else {
            result[season] = 1; // assigning 1 to the respective season key in result obj
        }
    })
    return result;
}

/**
 * 
 * The function calculates the number of matches won per team per year in IPL.
 * 
 * @param {Object[]} matches - takes array of object
 * 
 * @returns {Object} - matches won per team per year
 */
function matchesWonPerTeamPerYear(matches) {
    let result = {};

    // iterating through the matches array of object
    matches.forEach(({season, winner}) => {
        if (result[season]) { // checking that season key is having or not in result obj
            if (result[season][winner]) {
                result[season][winner] += 1; //incrementing the season winner count
            }else {
                result[season][winner] = 1; // assigning 1 to the respective season winner key in result obj
            }
        }else {
            result[season] = {}; // creating an object to the season
            result[season][winner] = 1; // assigning 1 to the respective season winner key in result obj
        }
    })
    return result;
}

/**
 * 
 * The function calculates the extra runs conceded per team in the year
 * 
 * @param {Object[]} matches - takes array of object
 * @param {Object[]} deliveries - takes array of object
 * @param {Number} year - takes year
 * 
 * @returns {Object} - extra runs per team in a year
 */
function extraRunsPerTeamByYear(matches, deliveries, year) {
    let result = {};

    matches
        .filter(({season}) => season == year) // filtering the required season of matches
        .forEach(({id}) => { // iterating through array of object
            deliveries
                    .filter(({match_id}) => id == match_id) // filtering matched id's
                    .forEach(({batting_team, extra_runs}) => { // iterating through array of object
                        if(result[batting_team]) {
                            result[batting_team] += Number(extra_runs); // adding extra runs 
                        }else {
                            result[batting_team] = Number(extra_runs); // assigning extra runs
                        }
                      })
        })

    return result;
}

/**
 * 
 * The function calculates the top 10 economical bowlers in the year
 * 
 * @param {Object[]} matches - takes array of object
 * @param {Object[]} deliveries - takes array of object
 * @param {Number} year - takes year
 * 
 * @returns {Object} - top 10 economical bowlers in a year
 */
function top10EconomicalBowlersByYear(matches, deliveries, year) {
    let bowlerInfo = {};
    let topEconomicalBowlers = [];
    let result = {};

    matches
        .filter(({season}) => season == year) // filtering the required season of matches
        .forEach(({id}) => { // iterating through array of object
            deliveries
                .filter(({match_id}) => id == match_id) // filtering matched id's
                .forEach(({bowler, total_runs}) => { // iterating through array of object
                    if(bowlerInfo[bowler]) {
                        bowlerInfo[bowler]['balls'] += 1;
                        bowlerInfo[bowler]['total_runs'] += Number(total_runs); // adding total runs
                     }else {
                         bowlerInfo[bowler] = {};
                         bowlerInfo[bowler]['total_runs'] = Number(total_runs); // assigning total runs
                         bowlerInfo[bowler]['balls'] = 1;
                     }
                })
        })

    Object.keys(bowlerInfo).forEach(bowler => { // iterating of objects 
        let overs = (bowlerInfo[bowler]['balls']) / 6; // calculating total overs
        let runs = bowlerInfo[bowler]['total_runs']; 
        let economyRate = runs / overs; // calculating economyRate
        topEconomicalBowlers.push([bowler, economyRate]); // pushing into an array
    })

    topEconomicalBowlers.sort((a, b) => a[1] - b[1]) // sorting of an array
                        .slice(0, 10) // slicing of an array from 0 to 10
                        .forEach(topEcoBowler => { // iterating though an array
                            result[topEcoBowler[0]] = topEcoBowler[1]
                        });


    return result;


}

/**
 * 
 * The function calculates the number of times each team won the toss and also won the match
 * 
 * @param {Object[]} matches - takes array of object
 * 
 * @returns {Object} - number of times each team won the toss and also match
 */
function eachTeamWonTheMatchAndToss(matches) {
    let result = {};

    // filtering the array
    matches.filter(({winner, toss_winner}) => winner == toss_winner)
            .forEach(({winner}) => { // iterating through the array
                if(result[winner]) {
                    result[winner] += 1;
                }else {
                    result[winner] = 1;
                }
            })

    return result;
}

/**
 * 
 * The function calculates a player who has won the highest number of Player of the Match awards for each season
 * 
 * @param {Object[]} matches - takes array of object
 * 
 * @returns {Object} - highest number of Player of the Match
 */
function highestNumberOfAwards(matches) {
    let result = {};
    let finalResult = {};
    
    // iterating through the matches array of object
    matches.forEach(({season, player_of_match}) => {
        if (result[season]) {
            if (result[season][player_of_match]) {
                result[season][player_of_match] += 1; // counting the player_of_match
            }else {
                result[season][player_of_match] = 1; // assigning 1 the player_of_match
            }
        }else {
            result[season] = {};
            result[season][player_of_match] = 1; // assigning 1 the player_of_match
        }
    })

    // checking who has more player_of_match award
    Object.keys(result).forEach(season => {
        let player = Object.keys(result[season])
                            .reduce((a, b) => result[season][a] > result[season][b] ? a : b);

        finalResult[season] = player;
    })

    return finalResult;
}

/**
 * 
 * The function calculates the strike rate of a batsman for each season
 * 
 * @param {Object[]} matches - takes array of object
 * @param {Object[]} deliveries - takes array of object
 * 
 * @returns {Object} - the strike rate of a batsman for each season
 */
function strikeRateOfBatsman(matches, deliveries) {
    let result = {};

    // iterating through the matches array of object
    matches.forEach(({id, season}) => {
        deliveries.filter(({match_id}) => id == match_id) // filtering the matched objects
                    .forEach(({batsman, total_runs}) => {
                        if(result[season]) {
                            if(result[season][batsman]) {
                                result[season][batsman]['balls'] += 1;
                                result[season][batsman]['total_runs'] += Number(total_runs);
                            }else {
                                result[season][batsman] = {};
                                result[season][batsman]['total_runs'] = Number(total_runs);
                                result[season][batsman]['balls'] = 1;
                            }
                        }else {
                            result[season] = {};
                            result[season][batsman] = {};
                            result[season][batsman]['total_runs'] = Number(total_runs);
                            result[season][batsman]['balls'] = 1;
                        }
                    })
    })

    // calculating strike rate of a batsman
    Object.keys(result).forEach(season => {
        Object.keys(result[season]).forEach(batsman => {
            let balls = result[season][batsman]['balls'];
            let runs = result[season][batsman]['total_runs'];
            let strikeRate = (runs / balls) * 100;
            result[season][batsman] = strikeRate;
        })
    })
    return result;
}

/**
 * 
 * The function calculates the highest number of times one player has been dismissed by another player
 * 
 * @param {Object[]} deliveries - takes array of object
 * 
 * @returns {Object} - highest number of times one player has been dismissed by another player
 */
function highestNumOfTimesPlayerDismissed(deliveries) {
    let result = {};

    // iterating through the matches array of object
    deliveries.forEach(({player_dismissed}) => {
        if(player_dismissed) {
            if(result[player_dismissed]){
                result[player_dismissed] += 1;
            }else {
                result[player_dismissed] = 1;
            }
        }
    })

    return result;
}

/**
 * 
 * The function calculates the bowler with the best economy in super overs
 * 
 * @param {Object[]} deliveries - takes array of object
 * 
 * @returns {Object} - the bowler with the best economy in super overs
 */
function bowlerWithBestEconomy(deliveries) {
    let result = {};
    let finalResult = {};

    deliveries.filter(({is_super_over}) => is_super_over == '1')
                .forEach(({bowler, total_runs}) => {
                    if(result[bowler]) {
                        result[bowler]['total_runs'] += Number(total_runs);
                        result[bowler]['balls'] += 1;
                    }else {
                        result[bowler] = {};
                        result[bowler]['total_runs'] = Number(total_runs);
                        result[bowler]['balls'] = 1;
                    }
                })
    
    // calculating best economy

    Object.keys(result).forEach(bowler => {
        let total_runs = result[bowler]['total_runs'];
        let balls = result[bowler]['balls'];
        let overs = balls / 6;
        let economyRate = total_runs / overs;
        result[bowler] = economyRate;
    })


    // getting the highest economy rate bowler name
    let player = Object.keys(result).reduce((a, b) => result[a] < result[b] ? a : b);
    finalResult[player] = result[player];
    return finalResult;
}


module.exports = {
    matchesPerYear,
    matchesWonPerTeamPerYear,
    extraRunsPerTeamByYear,
    top10EconomicalBowlersByYear,
    eachTeamWonTheMatchAndToss,
    highestNumberOfAwards,
    strikeRateOfBatsman,
    highestNumOfTimesPlayerDismissed,
    bowlerWithBestEconomy
}