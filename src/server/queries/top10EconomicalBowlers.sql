SELECT 
    (total_runs / overs) as economyRate,
    bowler
FROM (
    SELECT 
        SUM(total_runs) as total_runs,
        bowler,
        (COUNT(ball) / 6) as overs
    FROM matches m
    JOIN deliveries d
    ON m.id = d.match_id
    WHERE season = ?
    GROUP BY bowler
) ts
ORDER BY economyRate
LIMIT 10