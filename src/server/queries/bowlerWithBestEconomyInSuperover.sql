SELECT 
    (total_runs / overs) as economyRate,
    bowler
FROM (
    SELECT 
        bowler,
        (COUNT(ball) / 6) as overs,
        SUM(total_runs) as total_runs
    FROM deliveries
    WHERE is_super_over = 1
    GROUP BY bowler
) ts
ORDER BY economyRate
LIMIT 1