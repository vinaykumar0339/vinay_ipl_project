SELECT 
    COUNT(*) as won,
    winner
FROM matches
WHERE winner = toss_winner
GROUP BY winner