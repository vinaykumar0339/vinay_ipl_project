SELECT 
    season,
    COUNT(winner) as noOfTimes,
    winner
FROM matches
GROUP BY season, winner
ORDER BY season