SELECT 
    SUM(extra_runs) as extra_runs,
    batting_team
FROM matches m
JOIN deliveries d
ON m.id = d.match_id
WHERE season = ?
GROUP BY batting_team