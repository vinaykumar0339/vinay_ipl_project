SELECT  
	season,
	batsman,
	((total_runs) / balls)*100 as strikeRate
FROM  (SELECT 
    season,
    batsman,
    count(ball) as balls,
    sum(total_runs) as total_runs
FROM  matches m
JOIN  deliveries d
ON  m.id = d.match_id
GROUP BY season, batsman) ts
ORDER BY season