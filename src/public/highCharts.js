export function highChartsBar(id, seriesData, title, xTitle, yTitle) {
    Highcharts.chart(id, {
        chart: {
            type: 'column'
        },
        title: {
            text: title
        },
        subtitle: {
            text: 'Source: <a href="http://localhost:3000/output/matchesPerYear.json">Data</a>'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
            title: {
                text: xTitle
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: yTitle
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: `${yTitle}: <b>{point.y}</b>`
        },
        series: [{
            name: 'Matches',
            data: seriesData,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
}

export function highChartsBar2(id, category, seriesData, title, xTitle, yTitle) {
    Highcharts.chart(id, {
        chart: {
            type: 'column'
        },
        title: {
            text: title
        },
        subtitle: {
            text: 'Source: <a href="http://localhost:3000/output/matchesWonPerTeamPerYear.json">Data</a>'
        },
        xAxis: {
            categories: category,
            crosshair: true,
            title: {
                text: xTitle
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: yTitle
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} matches</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: seriesData
    });
}