export function formatData2(data) {
    let t = {}, a = -1;
    for (let year in data) {
        for (let teamName in a++, data[year]) {
            if (!t.hasOwnProperty(teamName)) {
                t[teamName] = [];
                for (let r = 0; r < a; r++) {
                    t[teamName].push(0)
                }
            }
            t[teamName].push(data[year][teamName])
        }
        for (let teamName in t) {
            t[teamName].length < a + 1 && t[teamName].push(0)
        }
    }
    return t;
}