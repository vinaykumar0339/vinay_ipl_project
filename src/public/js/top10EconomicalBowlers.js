const top10EconomicalBowlers = `http://localhost:3000/database/top10EconomicalBowlers?year=2015`;

import { highChartsBar } from '../highCharts.js';


fetch(top10EconomicalBowlers)
    .then(res => res.json())
    .then(data => {

        const id = 'top10EconomicalBowlers';
        const title = 'Top 10 Economical Bowlers In a Year';
        const xTitle = 'Bowler name';
        const yTitle = 'Economical rate';


        let seriesData = [];
        for (let d of data) {
            seriesData.push([d['bowler'], d['economyRate']])
        }

        highChartsBar(id, seriesData, title, xTitle, yTitle)
    });