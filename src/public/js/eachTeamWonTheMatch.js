const eachTeamWonTheMatch = `http://localhost:3000/database/eachTeamWonTheMatch`;

import { highChartsBar } from '../highCharts.js';

fetch(eachTeamWonTheMatch)
    .then(res => res.json())
    .then(data => {
        const id = 'eachTeamWonTheMatch';
        const title = 'Each Team Won The Toss and Also Won The Match';
        const xTitle = 'Team name';
        const yTitle = 'no Of times Won the both toss and match';

        let seriesData = [];
        for (let d of data) {
            seriesData.push([d['winner'], d['won']])
        }

        highChartsBar(id, seriesData, title, xTitle, yTitle);
    })