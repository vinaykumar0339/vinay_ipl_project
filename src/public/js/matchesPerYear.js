const matchesPerYear = `http://localhost:3000/database/matchesPerYear`;

import { highChartsBar } from '../highCharts.js';

fetch(matchesPerYear)
.then(res => res.json())
.then((data) => {
    const id = 'matchesPerYear';
    const title = 'Matches Per Year';
    const xTitle = 'Year';
    const yTitle = 'Number of matches';

    let seriesData = [];
    for (let d of data) {
        seriesData.push([d['year'], d['noOfMatches']]);
    }

    highChartsBar(id, seriesData, title, xTitle, yTitle)
})