const highestNumOfTimesPlayerDismissedForAllYear = `http://localhost:3000/database/highestNumOfTimesPlayerDismissedForAllYear`;

import { highChartsBar } from '../highCharts.js';


fetch(highestNumOfTimesPlayerDismissedForAllYear)
    .then(res => res.json())
    .then(data => {

        const id = 'highestNumOfTimesPlayerDismissedForAllYear';
        const title = 'Highest Number of Times Player Dismissed For All Years';
        const xTitle = 'Player name';
        const yTitle = 'Number of times dismissed';

        let seriesData = [];
        for (let d of data) {
            seriesData.push([d['player_dismissed'], d['noOfTimes']])
        }

        highChartsBar(id, seriesData, title, xTitle, yTitle);

    })