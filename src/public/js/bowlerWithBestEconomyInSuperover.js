const bowlerWithBestEconomyInSuperover = 'http://localhost:3000/database/bowlerWithBestEconomyInSuperover';

fetch(bowlerWithBestEconomyInSuperover)
    .then(res => res.json())
    .then(data => {
        let p = document.getElementById('bowlerNameInSuperOvers');
        p.innerHTML = `<b>${data[0]['bowler']}:</b> ${data[0]['economyRate'].toFixed(2)}`
    });