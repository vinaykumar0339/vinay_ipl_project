const matchesWonPerTeamPerYear = `http://localhost:3000/database/matchesWonPerTeamPerYear`;

import { highChartsBar2 } from '../highCharts.js';
import { formatData2 } from '../formatData.js';

fetch(matchesWonPerTeamPerYear)
    .then(res => res.json())
    .then((data) => {
        const id = 'matchesWonPerTeamPerYear';
        const title = 'Matches Won Per Team Per Year';
        const xTitle = 'Year';
        const yTitle = 'Number of Matches Won';

        let forData = {};
        data.forEach(obj => {
            let season = obj.season;
            if (forData[season]) {
                let winner = obj.winner;
                let noOfTimes = obj.noOfTimes;
                forData[season][winner] = noOfTimes;
            } else {
                let winner = obj.winner;
                let noOfTimes = obj.noOfTimes;
                forData[season] = {};
                forData[season][winner] = noOfTimes;
            }
        })

        let years = Object.keys(forData);
        let formattedData = formatData2(forData);

        let seriesData = [];
        for (let data in formattedData) {
            let obj = {
                name: data,
                data: formattedData[data]
            };
            seriesData.push(obj);
        }

        highChartsBar2(id, years, seriesData, title, xTitle, yTitle)
    })