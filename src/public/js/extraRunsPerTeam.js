const extraRunsPerTeam = `http://localhost:3000/database/extraRunsPerTeam?year=2016`;

import { highChartsBar } from '../highCharts.js';


fetch(extraRunsPerTeam)
    .then(res => res.json())
    .then((data) => {

        const id = 'extraRunsPerTeam';
        const title = 'Extra Runs Conceded Per Team In a Year';
        const xTitle = 'Team name';
        const yTitle = 'Number of extra runs';

        let seriesData = [];
        for (let d of data) {
            seriesData.push([d['batting_team'], d['extra_runs']])
        }

        highChartsBar(id, seriesData, title, xTitle, yTitle)
    })