const strikeRateOfBatsmanAllYears = `http://localhost:3000/database/strikeRateOfBatsmanAllYears`;

import { highChartsBar2 } from '../highCharts.js';
import { formatData2 } from '../formatData.js';


fetch(strikeRateOfBatsmanAllYears)
    .then(res => res.json())
    .then(data => {

        const id = 'strikeRateOfBatsmanAllYears';
        const title = 'Strike Rate of a Batsman For Each Year';
        const xTitle = 'Year';
        const yTitle = 'Strike Rate of a Batsman'

        let forData = {};
        data.forEach(obj => {
            let season = obj.season;
            if (forData[season]) {
                let batsman = obj.batsman;
                let strikeRate = obj.strikeRate;
                forData[season][batsman] = strikeRate;
            } else {
                let batsman = obj.batsman;
                let strikeRate = obj.strikeRate;
                forData[season] = {};
                forData[season][batsman] = strikeRate;
            }
        })


        let formattedData = formatData2(forData);
        let years = Object.keys(forData);
        let seriesData = [];
        for (let data in formattedData) {
            let obj = {
                name: data,
                data: formattedData[data]
            };
            seriesData.push(obj);
        }

        highChartsBar2(id, years, seriesData, title, xTitle, yTitle)

    });