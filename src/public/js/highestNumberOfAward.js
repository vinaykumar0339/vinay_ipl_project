const highestNumberOfAward = `http://localhost:3000/database/highestNumberOfAward`;


fetch(highestNumberOfAward)
    .then(res => res.json())
    .then(data => {
        let row1 = document.getElementById('row1');
        let row2 = document.getElementById('row2');

        let years = [];
        let players = [];

        for (let d of data) {
            years.push(d['season']);
            players.push(d['player_of_match']);
        }

        row1.insertCell(0).innerHTML = '<b>Year</b>';
        row2.insertCell(0).innerHTML = '<b>Player of the match</b>';
        for (let i = 0; i < years.length; i++) {
            row1.insertCell(i + 1).innerHTML = `${years[i]}`;
        }

        for (let i = 0; i < players.length; i++) {
            row2.insertCell(i + 1).innerHTML = `${players[i]}`;
        }
    });